import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { ToDoListComponent} from './ToDos/ToDo-list/ToDo-list.component';
import { ToDoCreateComponent} from './ToDos/ToDo-create/ToDo-create.component';
import {AuthGuard} from './auth/auth.guard';

const routes: Routes = [
  { path: '', component: ToDoListComponent , canActivate: [AuthGuard]},
  { path: 'create', component: ToDoCreateComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers : [AuthGuard]
})
export class AppRoutingModule {}
