import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { ToDoService } from '../ToDo.service';
import {ToDo} from '../ToDo.model';


@Component({
  selector: 'app-post-create',
  templateUrl: './ToDo-create.component.html',
  styleUrls: ['./ToDo-create.component.css']
})
export class ToDoCreateComponent implements OnInit {
  enteredTitle = '';
  enteredContent = '';
  toDo: ToDo;
  isLoading = false;
  form: FormGroup;
  private ToDoId: string;

  constructor(
    public toDoService: ToDoService,
    public route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      status: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(3)]
      }),
      description: new FormControl(null, {validators: [Validators.required]})
    });
  }
  onSaveToDo() {
    if (this.form.invalid) {
      return;
    }

    this.isLoading = true;
    this.toDoService.addToDo(
        this.form.value.status,
        this.form.value.description
    );
    this.form.reset();
  }
}
