import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

import { ToDo } from './ToDo.model';

@Injectable({ providedIn: 'root' })
export class ToDoService {
  private ToDo: ToDo[] = [];
  private ToDoUpdated = new Subject<{ ToDo: ToDo[]}>();
  constructor(private http: HttpClient, private router: Router) {}

  getToDos() {
    this.http
      .get<{ message: string; ToDos: any;}>(
        'https://todolist-task-back.herokuapp.com/users/getToDO'
      )
      .pipe(
        map(ToDoData => {
          return {
            toDo: ToDoData.ToDos.map( ToDos => {
              return {
                status: ToDos.status,
                description: ToDos.description,
                id: ToDos._id,
              };
            }),
            maxToDo: ToDoData.maxToDos
          };
        })
      )
      .subscribe(transformedToDoData => {
        this.ToDo = transformedToDoData.toDo;
        this.ToDoUpdated.next({
          ToDo: [...this.ToDo]
        });
      });

      }
      getToDpUpdateListener() {
      return this.ToDoUpdated.asObservable();
  }

  addToDo(status: string,  description: string) {
    const ToDoData = {
      'status' : status,
      'description': description
    } ;

    this.http
      .post<{ message: string; ToDo: ToDo }>(
        'https://todolist-task-back.herokuapp.com/users/addToDo',
        ToDoData
      )
      .subscribe(responseData => {
        this.router.navigate(['/']);
      });
  }
}
