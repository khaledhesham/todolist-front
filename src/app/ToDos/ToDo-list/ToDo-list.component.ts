import { Component, OnInit, OnDestroy } from '@angular/core';
import { PageEvent } from '@angular/material';
import { Subscription } from 'rxjs';

import { ToDo } from '../ToDo.model';
import { ToDoService } from '../ToDo.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './ToDo-list.component.html',
  styleUrls: ['./ToDo-list.component.css']
})
export class ToDoListComponent implements OnInit, OnDestroy {
  ToDos: ToDo[] = [];
  isLoading = false;
  private ToDosSub: Subscription;

  constructor(public postsService: ToDoService) {}

  ngOnInit() {
    this.isLoading = true;
    this.postsService.getToDos();
    this.ToDosSub = this.postsService
      .getToDpUpdateListener()
      .subscribe(  (ToDoData: { ToDo: ToDo[]}) => {
        this.isLoading = false;
        this.ToDos = ToDoData.ToDo;

      });
  }
  ngOnDestroy() {
   this.ToDosSub.unsubscribe();
  }


}
